#!/bin/bash

#SBATCH --job-name=relion_bax0158
#SBATCH --partition=std
#SBATCH --nodes=4
#SBATCH --tasks-per-node=1
#SBATCH --cpus-per-task=16
#SBATCH --time=12:00:00
# Never forget that! Strange happenings ensue otherwise.
#SBATCH --mail-user=bax0158@studium.uni-hamburg.de
#SBATCH --mail-type=ALL
#SBATCH --no-requeue
#SBATCH --export=ALL
source /sw/batch/init.sh
set -e
# Good Idea to stop operation on first error.
# Load environment modules for your application here.
# Actual work starting here. You might need to call
# srun or mpirun depending on your type of application
# for proper parallel work.
export OMP_NUM_THREADS=8
#export CUDA_VISIBLE_DEVICES=0
#export CUDA_MPS_ACTIVE_THREAD_PERCENTAGE=10
#export orte_base_help_aggregate=0
XXXcommandXXX

#/work/bax0158/relion/scripts/relion_it.py
