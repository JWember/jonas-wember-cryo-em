#!/bin/bash
# Nicht mehr das Original script sondern verändert mittels https://hpc.nih.gov/training/handouts/relion.pdf
#SBATCH --job-name=relion_bax0158
#SBATCH --partition=gpu
#SBATCH --nodes=1
#SBATCH --gres=gpu:0
#SBATCH --ntasks=XXXmpinodesXXX
#SBATCH --cpus-per-task=4
#SBATCH --mem=0
#SBATCH --time=12:00:00
# Never forget that! Strange happenings ensue otherwise.
#SBATCH --mail-user=bax0158@studium.uni-hamburg.de
#SBATCH --mail-type=ALL
#SBATCH --no-requeue
#SBATCH --export=ALL
source /sw/batch/init.sh
#source /home/bax0158/settings.sh 
set -e
# Good Idea to stop operation on first error.
# Load environment modules for your application here.
# Actual work starting here. You might need to call
# srun or mpirun depending on your type of application
# for proper parallel work.
export OMP_NUM_THREADS=8
echo $OMP_NUM_THREADS
export CUDA_VISIBLE_DEVICES=0
mpiexec -mca orte_forward_job_control 1 -n XXXmpinodesXXX  XXXcommandXXX
#mpiexec XXXcommandXXX
#srun --mpi=pmix XXXcommandXXX
