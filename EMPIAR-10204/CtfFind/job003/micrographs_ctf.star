
# version 30001

data_optics

loop_ 
_rlnOpticsGroupName #1 
_rlnOpticsGroup #2 
_rlnMtfFileName #3 
_rlnMicrographOriginalPixelSize #4 
_rlnVoltage #5 
_rlnSphericalAberration #6 
_rlnAmplitudeContrast #7 
_rlnMicrographPixelSize #8 
opticsGroup1            1 mtf_k2_200kV.star     0.885000   200.000000     1.400000     0.100000     0.885000 
 

# version 30001

data_micrographs

loop_ 
_rlnMicrographName #1 
_rlnOpticsGroup #2 
_rlnCtfImage #3 
_rlnDefocusU #4 
_rlnDefocusV #5 
_rlnCtfAstigmatism #6 
_rlnDefocusAngle #7 
_rlnCtfFigureOfMerit #8 
_rlnCtfMaxResolution #9 
MotionCorr/job022/Movies/20170629_00021_frameImage.mrc            1 CtfFind/job023/Movies/20170629_00021_frameImage_PS.ctf:mrc 10863.984375 10575.405273   288.579102    78.299301     0.131126     4.874623 
MotionCorr/job022/Movies/20170629_00022_frameImage.mrc            1 CtfFind/job023/Movies/20170629_00022_frameImage_PS.ctf:mrc  9836.612305  9586.711914   249.900391    70.310944     0.168904     3.619038 
MotionCorr/job022/Movies/20170629_00023_frameImage.mrc            1 CtfFind/job023/Movies/20170629_00023_frameImage_PS.ctf:mrc 10678.083984 10365.019531   313.064453    71.952339     0.166322     3.478493 
MotionCorr/job022/Movies/20170629_00024_frameImage.mrc            1 CtfFind/job023/Movies/20170629_00024_frameImage_PS.ctf:mrc 11692.697266 11353.693359   339.003906    74.329376     0.153701     3.495461 
MotionCorr/job022/Movies/20170629_00025_frameImage.mrc            1 CtfFind/job023/Movies/20170629_00025_frameImage_PS.ctf:mrc 10649.722656 10380.907227   268.815430    73.197975     0.156622     3.547374 
MotionCorr/job022/Movies/20170629_00026_frameImage.mrc            1 CtfFind/job023/Movies/20170629_00026_frameImage_PS.ctf:mrc  8344.731445  8101.462402   243.269043    72.663071     0.187280     3.088662 
MotionCorr/job022/Movies/20170629_00027_frameImage.mrc            1 CtfFind/job023/Movies/20170629_00027_frameImage_PS.ctf:mrc  9144.671875  8826.818359   317.853516    78.421661     0.173929     3.227791 
MotionCorr/job022/Movies/20170629_00028_frameImage.mrc            1 CtfFind/job023/Movies/20170629_00028_frameImage_PS.ctf:mrc 10159.593750  9805.610352   353.983398    75.564636     0.164021     3.088662 
MotionCorr/job022/Movies/20170629_00029_frameImage.mrc            1 CtfFind/job023/Movies/20170629_00029_frameImage_PS.ctf:mrc  8303.088867  7964.729980   338.358887    79.299423     0.193193     3.332882 
MotionCorr/job022/Movies/20170629_00030_frameImage.mrc            1 CtfFind/job023/Movies/20170629_00030_frameImage_PS.ctf:mrc  9116.635742  8828.368164   288.267578    76.932800     0.202325     3.380045 
MotionCorr/job022/Movies/20170629_00031_frameImage.mrc            1 CtfFind/job023/Movies/20170629_00031_frameImage_PS.ctf:mrc 10123.698242  9808.523438   315.174805    75.420326     0.198311     3.198971 
MotionCorr/job022/Movies/20170629_00035_frameImage.mrc            1 CtfFind/job023/Movies/20170629_00035_frameImage_PS.ctf:mrc  9292.183594  8923.719727   368.463867    75.859329     0.150948     3.227791 
MotionCorr/job022/Movies/20170629_00036_frameImage.mrc            1 CtfFind/job023/Movies/20170629_00036_frameImage_PS.ctf:mrc 10073.214844  9709.629883   363.584961    72.993103     0.160535     3.184754 
MotionCorr/job022/Movies/20170629_00037_frameImage.mrc            1 CtfFind/job023/Movies/20170629_00037_frameImage_PS.ctf:mrc 11029.849609 10743.727539   286.122070    77.507263     0.141154     3.227791 
MotionCorr/job022/Movies/20170629_00039_frameImage.mrc            1 CtfFind/job023/Movies/20170629_00039_frameImage_PS.ctf:mrc 10135.966797  9799.805664   336.161133    78.797073     0.164632     3.287016 
MotionCorr/job022/Movies/20170629_00040_frameImage.mrc            1 CtfFind/job023/Movies/20170629_00040_frameImage_PS.ctf:mrc 11115.500977 10829.664062   285.836914    80.524971     0.169805     3.102033 
MotionCorr/job022/Movies/20170629_00042_frameImage.mrc            1 CtfFind/job023/Movies/20170629_00042_frameImage_PS.ctf:mrc 10108.360352  9801.257812   307.102539    77.232460     0.197019     3.396064 
MotionCorr/job022/Movies/20170629_00043_frameImage.mrc            1 CtfFind/job023/Movies/20170629_00043_frameImage_PS.ctf:mrc 11089.771484 10819.356445   270.415039    73.000664     0.211310     3.075406 
MotionCorr/job022/Movies/20170629_00044_frameImage.mrc            1 CtfFind/job023/Movies/20170629_00044_frameImage_PS.ctf:mrc 11456.396484 11169.147461   287.249023    71.860565     0.145244     3.317452 
MotionCorr/job022/Movies/20170629_00045_frameImage.mrc            1 CtfFind/job023/Movies/20170629_00045_frameImage_PS.ctf:mrc 12109.835938 11827.956055   281.879883    75.552490     0.155060     3.170662 
MotionCorr/job022/Movies/20170629_00046_frameImage.mrc            1 CtfFind/job023/Movies/20170629_00046_frameImage_PS.ctf:mrc 13065.083008 12759.353516   305.729492    78.985680     0.157128     3.242396 
MotionCorr/job022/Movies/20170629_00047_frameImage.mrc            1 CtfFind/job023/Movies/20170629_00047_frameImage_PS.ctf:mrc 11486.020508 11198.335938   287.684570    69.669197     0.144646     3.272007 
MotionCorr/job022/Movies/20170629_00049_frameImage.mrc            1 CtfFind/job023/Movies/20170629_00049_frameImage_PS.ctf:mrc 13119.303711 12777.777344   341.526367    73.694679     0.166613     3.287016 
 
