
# version 30001

data_optics

loop_ 
_rlnOpticsGroupName #1 
_rlnOpticsGroup #2 
_rlnMtfFileName #3 
_rlnMicrographOriginalPixelSize #4 
_rlnVoltage #5 
_rlnSphericalAberration #6 
_rlnAmplitudeContrast #7 
_rlnMicrographPixelSize #8 
opticsGroup1            1 mtf_k2_200kV.star     0.885000   200.000000     1.400000     0.100000     0.885000 
 

# version 30001

data_micrographs

loop_ 
_rlnCtfPowerSpectrum #1 
_rlnMicrographName #2 
_rlnMicrographMetadata #3 
_rlnOpticsGroup #4 
_rlnAccumMotionTotal #5 
_rlnAccumMotionEarly #6 
_rlnAccumMotionLate #7 
MotionCorr/job022/Movies/20170629_00021_frameImage_PS.mrc MotionCorr/job022/Movies/20170629_00021_frameImage.mrc MotionCorr/job022/Movies/20170629_00021_frameImage.star            1    16.417004     2.499067    13.917937 
MotionCorr/job022/Movies/20170629_00022_frameImage_PS.mrc MotionCorr/job022/Movies/20170629_00022_frameImage.mrc MotionCorr/job022/Movies/20170629_00022_frameImage.star            1    19.546053     2.473967    17.072087 
MotionCorr/job022/Movies/20170629_00023_frameImage_PS.mrc MotionCorr/job022/Movies/20170629_00023_frameImage.mrc MotionCorr/job022/Movies/20170629_00023_frameImage.star            1    17.530987     1.943724    15.587262 
MotionCorr/job022/Movies/20170629_00024_frameImage_PS.mrc MotionCorr/job022/Movies/20170629_00024_frameImage.mrc MotionCorr/job022/Movies/20170629_00024_frameImage.star            1    18.101656     1.726248    16.375408 
MotionCorr/job022/Movies/20170629_00025_frameImage_PS.mrc MotionCorr/job022/Movies/20170629_00025_frameImage.mrc MotionCorr/job022/Movies/20170629_00025_frameImage.star            1    24.127048     3.569004    20.558044 
MotionCorr/job022/Movies/20170629_00026_frameImage_PS.mrc MotionCorr/job022/Movies/20170629_00026_frameImage.mrc MotionCorr/job022/Movies/20170629_00026_frameImage.star            1    13.178824     1.829554    11.349270 
MotionCorr/job022/Movies/20170629_00027_frameImage_PS.mrc MotionCorr/job022/Movies/20170629_00027_frameImage.mrc MotionCorr/job022/Movies/20170629_00027_frameImage.star            1    15.077192     1.435057    13.642134 
MotionCorr/job022/Movies/20170629_00028_frameImage_PS.mrc MotionCorr/job022/Movies/20170629_00028_frameImage.mrc MotionCorr/job022/Movies/20170629_00028_frameImage.star            1    13.776374     1.103743    12.672631 
MotionCorr/job022/Movies/20170629_00029_frameImage_PS.mrc MotionCorr/job022/Movies/20170629_00029_frameImage.mrc MotionCorr/job022/Movies/20170629_00029_frameImage.star            1    16.175998     1.524444    14.651553 
MotionCorr/job022/Movies/20170629_00030_frameImage_PS.mrc MotionCorr/job022/Movies/20170629_00030_frameImage.mrc MotionCorr/job022/Movies/20170629_00030_frameImage.star            1    16.752184     1.881932    14.870251 
MotionCorr/job022/Movies/20170629_00031_frameImage_PS.mrc MotionCorr/job022/Movies/20170629_00031_frameImage.mrc MotionCorr/job022/Movies/20170629_00031_frameImage.star            1    13.608600     1.503745    12.104855 
MotionCorr/job022/Movies/20170629_00035_frameImage_PS.mrc MotionCorr/job022/Movies/20170629_00035_frameImage.mrc MotionCorr/job022/Movies/20170629_00035_frameImage.star            1    15.650392     1.685245    13.965147 
MotionCorr/job022/Movies/20170629_00036_frameImage_PS.mrc MotionCorr/job022/Movies/20170629_00036_frameImage.mrc MotionCorr/job022/Movies/20170629_00036_frameImage.star            1    12.990951     0.780684    12.210267 
MotionCorr/job022/Movies/20170629_00037_frameImage_PS.mrc MotionCorr/job022/Movies/20170629_00037_frameImage.mrc MotionCorr/job022/Movies/20170629_00037_frameImage.star            1    13.654905     1.415354    12.239551 
MotionCorr/job022/Movies/20170629_00039_frameImage_PS.mrc MotionCorr/job022/Movies/20170629_00039_frameImage.mrc MotionCorr/job022/Movies/20170629_00039_frameImage.star            1    12.166438     1.465686    10.700752 
MotionCorr/job022/Movies/20170629_00040_frameImage_PS.mrc MotionCorr/job022/Movies/20170629_00040_frameImage.mrc MotionCorr/job022/Movies/20170629_00040_frameImage.star            1    13.666367     1.250745    12.415622 
MotionCorr/job022/Movies/20170629_00042_frameImage_PS.mrc MotionCorr/job022/Movies/20170629_00042_frameImage.mrc MotionCorr/job022/Movies/20170629_00042_frameImage.star            1    15.119874     1.152630    13.967244 
MotionCorr/job022/Movies/20170629_00043_frameImage_PS.mrc MotionCorr/job022/Movies/20170629_00043_frameImage.mrc MotionCorr/job022/Movies/20170629_00043_frameImage.star            1    13.542213     1.356002    12.186212 
MotionCorr/job022/Movies/20170629_00044_frameImage_PS.mrc MotionCorr/job022/Movies/20170629_00044_frameImage.mrc MotionCorr/job022/Movies/20170629_00044_frameImage.star            1    15.256305     1.772670    13.483635 
MotionCorr/job022/Movies/20170629_00045_frameImage_PS.mrc MotionCorr/job022/Movies/20170629_00045_frameImage.mrc MotionCorr/job022/Movies/20170629_00045_frameImage.star            1    10.398699     1.085795     9.312904 
MotionCorr/job022/Movies/20170629_00046_frameImage_PS.mrc MotionCorr/job022/Movies/20170629_00046_frameImage.mrc MotionCorr/job022/Movies/20170629_00046_frameImage.star            1    11.548495     0.911378    10.637116 
MotionCorr/job022/Movies/20170629_00047_frameImage_PS.mrc MotionCorr/job022/Movies/20170629_00047_frameImage.mrc MotionCorr/job022/Movies/20170629_00047_frameImage.star            1    13.528753     0.803311    12.725442 
MotionCorr/job022/Movies/20170629_00049_frameImage_PS.mrc MotionCorr/job022/Movies/20170629_00049_frameImage.mrc MotionCorr/job022/Movies/20170629_00049_frameImage.star            1    13.497510     1.426007    12.071503 
 
