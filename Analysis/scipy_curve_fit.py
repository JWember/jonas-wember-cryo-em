#from tarfile import LENGTH_PREFIX
from cProfile import label
from calendar import c
from turtle import color
from unittest import result
from scipy.stats import rv_continuous, norm, beta, kstest
from scipy.optimize import curve_fit
from cv2 import mean
import numpy as np
from matplotlib import pyplot as plt, testing
from csv import reader
import time
from matplotlib.backends.backend_pdf import PdfPages
import pandas as pd
import math
import collections
from sklearn.cluster import KMeans

output = PdfPages('scipy_dist_on_histos_full.pdf')


def mix_dist(skip_lines = 126):
    with open('density.csv', 'r') as read_obj:
        csv_reader = reader(read_obj)
        #header = next(csv_reader)
        for i in range(0,skip_lines):
            header = next(csv_reader)
            header = next(csv_reader)
        header = next(csv_reader)

        #if header != None:
        for row in csv_reader:
            
            #fig, axes = plt.subplots(nrows=2, ncols=2, figsize=(16, 9))
            #axes = axes.flatten()
            header = next(csv_reader)

            row = list(map(float, row))
            x = np.array(row) 
            x = np.array([item for item in x if item >=0])
            x /= max(x)


            yield header, x

            #Alle plots sind zusammen und jetzt sind glaub am ehesten noch die x achse falsch 


def normal(x,a ,b , c): # 0.38112289 0.022595
    return a * np.exp(-b * x) + c  


def quadratisch(x,pot,a,c):
    return a*(x**pot)+c

def dicts_plot():
    plotting = True
    use_clustering = True
    skip_lines = 1
    end_line = 127
    data = mix_dist(skip_lines=skip_lines)
    official = np.linspace(skip_lines ,end_line ,end_line - skip_lines)
    whole = np.linspace(0,1,2000 )[1:]
    x_height_list = list()
    c_bucket = list()

    a_bucket = list()
    b_bucket = list()
    for count in range(skip_lines,127):
        start_time = time.time()
        header,x=next(data)
        print(header)

        crossing_dict = {i:np.count_nonzero(x==i) for i in x}
        crossing_dict = collections.OrderedDict(sorted(crossing_dict.items()))

        crossing_dict_items = crossing_dict.items()
        crossing_dict_keys = np.array(list(crossing_dict.keys()))
        crossing_dict_values = np.array(list(crossing_dict.values()))

        last_key = crossing_dict_keys[-1]
        if use_clustering:

            x_dict = dict()
            nuller = np.zeros(len(list(crossing_dict.keys())))
            dict_keys = np.array(list(crossing_dict.keys()))
            n_cluster = int(np.ceil(len(dict_keys)/2))
            if int(np.ceil(len(crossing_dict_keys)/2)) > 500:
                n_cluster = 500

            
            kmeans = KMeans(n_clusters=n_cluster, random_state=0).fit(list(zip(dict_keys,nuller)))
            previous = -1
            for count, cluster in enumerate(kmeans.labels_):
                if cluster != previous:
                    previous = cluster
                    key = dict_keys[count]
                    x_dict.update({key : 0})
                x_dict[key] +=  crossing_dict[dict_keys[count]]

        else:
            x_dict = crossing_dict

        x_dict_items = x_dict.items()
        x_dict_keys = np.array(list(x_dict.keys()))
        x_dict_values = np.array(list(x_dict.values()))
        

        bins = x_dict_keys

        if plotting:
            fig, axes = plt.subplots(nrows=2, ncols=2, figsize=(16, 9))
            axes = axes.flatten()

            axes[0].bar(x_dict_keys, x_dict_values , color='red', width=0.001)

        else:
            plot1 = plt.figure()

            plt.bar(x_dict_keys, x_dict_values, color='red')

            plt.close(plot1)
        

        count_x= x_dict_values

        weights = np.ones(len(bins))
        weights = weights * 0.01
        popt, pcov = curve_fit(normal, x_dict_keys, x_dict_values, sigma = weights, maxfev= 3000)#, absolute_sigma= True)#, sigma=(np.linspace(1,20,len(bins)))+1)


        residuals = x_dict_values- normal(x_dict_keys, *popt)
        ss_res = np.sum(residuals**2)
        ss_tot = np.sum((x_dict_values-np.mean(x_dict_values))**2)
        r_squared = 1 - (ss_res / ss_tot)

        pdf_dist = normal(bins, *popt) / sum(normal(bins, *popt))
        cdf_dist = np.cumsum(pdf_dist)

        pdf_x = count_x / sum(count_x)
        cdf_x = np.cumsum(pdf_x)

        if plotting:
            axes[0].set_title('Distribution, shown as histogram')
            axes[0].plot(whole, normal(whole, *popt), label = 'Weight Function', color = 'blue')
            axes[0].set_xlabel('Intensity')
            axes[0].set_ylabel('Amount of data')
            axes[0].set_xlim(left = 0)
            axes[0].set_ylim(bottom = 0)
            axes[0].legend()
            axes[2].set_title('Probability Distribution Function')
            axes[2].plot(bins, pdf_x, color="red", label="PDF_data_x")
            axes[2].plot(bins, pdf_dist, color="blue", label="PDF_dist")
            axes[2].set_xlabel('Intensity')
            axes[2].set_ylabel('Probability')
            axes[2].set_xlim(left = 0)
            axes[2].set_ylim(bottom = 0)
            axes[2].legend()                
            axes[3].set_title('Cumulative Distribution Function')
            axes[3].set_xlabel('Intensity')
            axes[3].set_ylabel('Cumulative Probability')
            axes[3].plot(bins, cdf_x, color='red', label="CDF_data_x")
            axes[3].plot(bins, cdf_dist, color='blue', label="CDF_dist")
            axes[3].set_xlim(left = 0)
            axes[3].set_ylim(bottom = 0)
            axes[3].legend()
            axes[1].set_title('Distribution, shown as plot')
            axes[1].set_xlabel('Intensity')
            axes[1].set_ylabel('Amount of data')
            axes[1].plot(bins, count_x, color='red', marker = '+', label='Hist Data')
            axes[1].plot(whole, normal(whole, *popt), label = 'Weight Function', color = 'blue')
            axes[1].set_xlim(left = 0)
            axes[1].set_ylim(bottom = 0)
            axes[1].legend()

        

            for ax in axes:
                ax.grid(True)
            
            output.savefig()
            plt.close(fig)
            #plt.close(plot1)
        print(time.time()-start_time)
        a_bucket.append(r_squared)
        b_bucket.append(popt[1])
        c_bucket.append(popt[2])
        x_height_list.append(count_x)
        

    #fig = plt.figure(figsize=(16,9))
    #print(difference_list)
    if plotting:
        output.close()

    a_popt, pcov = curve_fit(normal, official,a_bucket)
    #print(weight_popt, pcov)
    print([round(num,3) for num in a_bucket])
    print([round(num,3) for num in b_bucket])
    print([round(num,3) for num in c_bucket])

    plt.title('Residual sum of squares')
    plt.plot(official, normal(official, *a_popt), label = 'Approximation')
    plt.plot(official,a_bucket, label= 'RSS')
    plt.xlabel('Shells')
    plt.ylabel('R squared')
    plt.legend()
    plt.savefig('R_squared')
    plt.show()

    b_popt, pcov = curve_fit(quadratisch, official, b_bucket)
    plt.title('Variable b in a*exp(x*b)+c')
    plt.plot(official, quadratisch(official, *b_popt), label = 'Fitted Function')
    plt.plot(official,b_bucket, label= 'b parameter ')
    plt.xlabel('Shells')
    plt.ylabel('b value')
    plt.legend()
    plt.savefig('expo b values')
    plt.show()

    c_popt, pcov = curve_fit(normal, official,c_bucket)
    plt.title('Variable c in a*exp(x*b)+c')
    plt.plot(official, normal(official, *c_popt), label = 'Fitted Function')
    plt.plot(official,c_bucket, label= 'c parameter')
    plt.xlabel('Shells')
    plt.ylabel('c value')
    plt.legend()
    plt.savefig('expo c values')
    plt.show()

    
    
def percentage_plot():
    x = mix_dist(skip_lines=0)
    d = list()
    percent03 = list()
    percent04 = list()
    percent05 = list()
    percent06 = list()
    percent07 = list()
    percent08 = list()
    percent09 = list()
    for h in range(0,126):
        header,data=next(x)
        list03 = [item for item in data if item > 0.3]
        list04 = [item for item in data if item > 0.4]
        list05 = [item for item in data if item > 0.5]
        list06 = [item for item in data if item > 0.6]
        list07 = [item for item in data if item > 0.7]
        list08 = [item for item in data if item > 0.8]
        list09 = [item for item in data if item > 0.9]
        datalength = len(data)
        percent03.append(len(list03) / datalength)
        percent04.append(len(list04) / datalength)
        percent05.append(len(list05) / datalength)
        percent06.append(len(list06) / datalength)
        percent07.append(len(list07) / datalength)
        percent08.append(len(list08) / datalength)
        percent09.append(len(list09) / datalength)
        d.append(len(data))

        print(len(data))
        
    plt.title('Amount of datapoints over threshold')
    plt.plot(percent05, label = 'Intensity > 0.5')
    plt.plot(percent06, label = 'Intensity > 0.6')
    plt.plot(percent07, label = 'Intensity > 0.7')
    plt.plot(percent08, label = 'Intensity > 0.8')
    plt.plot(percent09, label = 'Intensity > 0.9')
    plt.xlabel('Shells')
    plt.ylabel('Percent of whole data')
    plt.legend()
    plt.savefig('dataamount.png', dpi=300)
    plt.show()
    time.sleep(50)
    plt.plot(d)
    plt.xlabel('Shells')
    plt.ylabel('Number of intensity data points')
    plt.savefig('amount of intensity data')
    plt.show()


if __name__ == "__main__":
    dicts_plot()
    percentage_plot()
    
